<?php

return [
    'servers' => [
        'ru' => [
            'api_domain' => 'https://api.umnico.com',
            'front_domain' => 'https://umnico.com',
            'matcher' => 'https://(api)\.umnico\.com',
            'locale' => 'ru',
            // ???
            'currency' => 'RUB',
        ],

        // ???
        'support' => [
            'api_domain' => '',
            'front_domain' => '',
            'matcher' => '',
            'locale' => 'ru',
            'currency' => 'RUB',
        ],
    ],

    'events' => [
        'inboxFlags' => [
            'newChatCreated' => false,
            'newTicketOpened' => false,
            'externalPostComment' => true,
        ],
        'inboxReceived' => true,
        'outboxSent' => true,
        'newContactChatAdded' => false,
        'chatTicketClosed' => true,
        'chatTransferred' => false,
        'chatTagAttached' => false,
        'chatTagDetached' => false,
        'chatTicketTagAttached' => false,
        'chatTicketTagDetached' => false,
        'messageStatusChanged' => false,
        'contactDataUpdated' => true,
    ],

    'messenger_types' => [
        'default' => [
            'inline_buttons' => false,
            'quick_replies' => false,

            'text' => [
                // ??? Umnico doesn't seem to have message length specification
                'max_length' => 4096,
            ],

            'image' => [
                'enabled' => true,
                'mime' => ['image/jpeg', 'image/png', 'image/gif'],
                'max_file_size' => 5 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'video' => [
                'enabled' => true,
                'mime' => ['video/mp4', 'video/3gpp'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'document' => [
                'enabled' => true,
                'mime' => ['*'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'audio' => [
                'enabled' => true,
                'mime' => ['audio/ogg', 'audio/mpeg', 'audio/mp4', 'audio/aac'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => false,
                'caption_max_length' => 1000,
            ],

            'voice' => [
                'enabled' => false,
                'mime' => ['audio/ogg'],
                'max_file_size' => 1024 * 1024,
            ],

            'geolocation' => ['enabled' => false],
        ],

        // ??? how do we map group & messenger to internal types
        'fb_group' => [
            'extends' => 'default',
            'name' => 'Facebook (group)',
        ],

        'fb_messenger' => [
            'extends' => 'default',
            'internal_type' => 'facebook',
            'name' => 'Facebook (messenger)',
        ],

        // exists, but is not described in docs
        'discord' => [
            'extends' => 'default',
            'name' => 'Discord'
        ],

        'vk_group' => [
            'extends' => 'default',
            'internal_type' => 'vk',
            'name' => 'Группа Вконтакте',
        ],

        'telebot' => [
            'extends' => 'default',
            'name' => 'Telegram',
            'internal_type' => 'telegram',
        ],

        'telegram' => [
            'extends' => 'default',
            'name' => 'Telegram User'
        ],

        'whatsapp2' => [
            'extends' => 'default',
            'name' => 'Whatsapp',
            'internal_type' => 'whatsapp',
        ],

        'waba' => [
            'extends' => 'default',
            'name' => 'Whatsapp (business API)',
        ],

        'widget' => [
            'extends' => 'default',
            'name' => 'External channel',
        ],

        'instagramV3' => [
            'extends' => 'default',
            'name' => 'Instagram',
            'internal_type' => 'instagram',
        ],

        'viber_bot' => [
            'extends' => 'default',
            'name' => 'Viber',
            'internal_type' => 'viber_public'
        ],

        'ok' => [
            'extends' => 'default',
            'name' => 'Одноклассники',
        ],

        'avito' => [
            'extends' => 'default',
            'name' => 'Avito',
        ],

        'mailbox' => [
            'extends' => 'default',
            'name' => 'Mailbox',
        ],
    ]
];
