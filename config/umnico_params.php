<?php

return [
    'custom_id' => env('UMNICO_CUSTOM_ID'),
    'messenger_channels' => [
        'fb_group' => [
            'comment',
        ],

        'fb_messenger' => [
            'message',
        ],

        'discord' => [
            'message',
        ],

        'vk_group' => [
            'message',
            'comment',
        ],

        'telebot' => [
            'message',
        ],

        'telegram' => [
            'message',
        ],

        'whatsapp2' => [
            'message',
        ],

        'waba' => [
            'message',
        ],

        'widget' => [
            'message',
        ],

        'instagramV3' => [
            'message',
            'comment',
        ],

        'viber_bot' => [
            'message',
        ],

        'ok' => [
            'message',
        ],

        'avito' => [
            'comment',
        ],

        'mailbox' => [
            'message',
        ],
    ]
];
