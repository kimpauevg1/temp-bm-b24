<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/umnico.php', 'umnico');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
        $this->loadViewsFrom(__DIR__ . '/Resources/Views', 'umnico');

        $this->makeHub()->registerAppType('umnico', fn (AppInstance $app) => new AppHandler($app, $this->app['config']));
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }
}
