<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Requests\BaseMessageRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use Illuminate\Support\Facades\Log;

use JetBrains\PhpStorm\ArrayShape;

use function with;

trait MessageCommands
{
    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        Log::error(__METHOD__ . ' is not supported for Umnico.');
        throw new ErrorException(ErrorCode::FeatureNotSupported, 'Action [' . __METHOD__ . '] is not supported for Umnico.');
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return $this->sendMessageToClient($request, ['text' => $request->text]);
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        $media = $this->getUmnicoMedia($request);
        return $this->sendMessageToClient($request, [
            ...($request->caption ? ['text' => $request->caption] : []),
            ...['attachment' => $media],
        ]);
    }

    #[ArrayShape([
        'type' => 'string',
        'media' => 'array',
    ])]
    protected function getUmnicoMedia(SendMediaRequest $request): array
    {
        $multipart = $this->formMultipartArrayForMediaUpload($request);
        return $this->module->getApiCommands()->uploadMedia($multipart);
    }

    protected function formMultipartArrayForMediaUpload(SendMediaRequest $request): array
    {
        $media = $this->formMultipartGuzzleArrayForFile($request->file);
        $source = [
            'name' => 'source',
            'contents' => data_get($request->chat->getExtraData(), ExtraDataProps::CURRENT_REAL_ID),
        ];
        return [$source, $media];
    }

    #[ArrayShape([
        'name' => 'string',
        'filename' => 'string',
        'Mime-Type' => 'null|string',
        'contents' => 'string'
    ])]
    protected function formMultipartGuzzleArrayForFile(MediaFile $file): array
    {
        try {
            $content = file_get_contents($file->url);
        } catch (\Exception $e) {
            Log::error('Could not get contents from url ' . $file->url . ' Exception message: ' . $e->getMessage());
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not fetch file contents from provided URL');
        }
        if (!$content) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not fetch file contents from provided URL');
        }
        $mime = $file->mime ?? ($this->getFileMime($content) ?: null);
        $filename = $this->generateFileName($file->type, $mime);
        return array_filter([
            'name' => 'media',
            'filename' => $filename,
            'Mime-Type' => $mime,
            'contents' => $content,
        ]);
    }

    protected function generateFileName(MediaFileType $type, null|string $mime): string
    {
        if ($mime) {
            return $type->name . '.' . explode('/', $mime)[1];
        }
        return $type->name;
    }

    protected function getFileMime(string $content): false|string
    {
        $finfo = finfo_open(flags: FILEINFO_MIME_TYPE);
        $mime = finfo_buffer(finfo: $finfo, string: $content, flags: FILEINFO_MIME_TYPE);
        finfo_close($finfo);
        return $mime;
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function sendMessage(Chat $chat, array $payload): MessageSendResult
    {
        return with(
            $this->module->getApiCommands()->sendMessage($chat, $payload),
            fn($resp) => new MessageSendResult($resp[0]['messageId'], data_get($resp, '0.source.id'))
        );
    }

    protected function sendMessageToClient(BaseMessageRequest $request, array $payload): MessageSendResult
    {
        // TODO: check if Umnico supports inline buttons payload.
        return $this->sendMessage(
            $request->chat,
            $payload
        );
    }
}
