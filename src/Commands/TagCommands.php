<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\ExtraDataProps;

trait TagCommands
{
    public function attachTagToChat(ChatTagRequest $request): void
    {
        if (!$request->forTicket) {
            throw new ErrorException(ErrorCode::FeatureNotSupported, 'Umnico supports tags for requests only');
        }
        $leadId = data_get($request->chat->getExtraData(), ExtraDataProps::LEAD_ID);

        if (!$leadId) {
            throw new ErrorException(ErrorCode::DataMissing, 'Could not retrieve leadId from chat');
        }

        $this->module->getApiCommands()->addTagToLead($leadId, $request->tag->getLabel());
    }

    public function detachTagFromChat(ChatTagRequest $request): void
    {
        if (!$request->forTicket) {
            throw new ErrorException(ErrorCode::FeatureNotSupported, 'Umnico supports tags for requests only');
        }
        $leadId = data_get($request->chat->getExtraData(), ExtraDataProps::LEAD_ID);

        if (!$leadId) {
            throw new ErrorException(ErrorCode::DataMissing, 'Could not retrieve leadId from chat');
        }

        $this->module->getApiCommands()->deleteTagFromLead($leadId, $request->tag->getLabel());
    }
}
