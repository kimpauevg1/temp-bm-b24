<?php

namespace BmPlatform\Umnico\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\ExtraDataProps;

trait OperatorCommands
{
    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        if ($request->target instanceof OperatorGroup) {
            throw new ErrorException(
                ErrorCode::InternalServerError,
                'Umnico doesn\'t have a representation of OperatorGroup'
            );
        }

        $leadId = $request->chat->getExtraData()[ExtraDataProps::LEAD_ID];
        $operator = $this->module->getApiCommands()->getOperator($request->target->getExternalId());

        if (!$operator || !$leadId) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not get the operator from Umnico');
        }

        // TODO: we might wanna do some permission checks
        $response = $this->module->getApiCommands()->changeLead($leadId, ['userId' => $operator['id']]);

        if (!$response) {
            throw new ErrorException(ErrorCode::UnexpectedServerError, 'Could not change the operator on Umnico');
        }

        return new NewOperatorResponse((string)$operator['id']);
    }
}
