<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\Interfaces\Hub;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SetWebhook
{
    public function __construct(public readonly AppHandler $handler, public readonly Hub $hub)
    {
        //
    }

    public function __invoke($enabled = true)
    {
        if (App::isLocal()) return;

        $url = URL::route('umnico.callback', [ 'callbackId' => $this->hub->getCallbackId($this->handler->user) ]);

        $apiClient = $this->handler->getApiClient();

        $webhook = collect($apiClient->get('webhooks'))->firstWhere('url', $url);

        if ($webhook && $enabled) return;

        $data = [
            'url' => $url
        ];

        if ($webhook) {
            $apiClient->delete('webhooks/:id', [ 'params' => [ 'id' => $webhook['id'] ]]);
        } else {
            $apiClient->post('webhooks', [ 'json' => $data ]);
        }
    }
}
