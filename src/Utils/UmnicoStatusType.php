<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\Enums\MediaFileType;

enum UmnicoStatusType
{
//active - активный тип
//completed - архивный успешный тип
//failed - архивный неуспешный тип
//ignore - тип «Не обращение»
    case active;
    case completed;
    case failed;
    case ignore;

    public static function isActive(string $type): bool
    {
        return $type === self::active->name;
    }
}
