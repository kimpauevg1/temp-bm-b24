<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\AppHandler;
use Carbon\Carbon;
use JetBrains\PhpStorm\ArrayShape;

class DataWrap implements \ArrayAccess
{
    public array $source;

    public function __construct(public readonly AppHandler $module, public readonly array $data)
    {
        $this->setSource();
    }

    public function messengerInstance(): string
    {
        return data_get($this->data, 'message.sa.id') ?? data_get($this->data, 'lead.socialAccount.id');
    }


    public function chat(): Chat
    {
        if (!$this->source) {
            throw new ErrorException(ErrorCode::DataMissing, 'Cannot initialize chat! Failed to resolve [source]');
        }
        $messengerId = data_get($this->source, 'id');
        $sources = $this->module->getApiCommands()->getSources(data_get($this->data, 'leadId'));
        if (!$sources) {
            throw new ErrorException(ErrorCode::DataMissing, 'Cannot initialize chat! Failed to retrieve [sources]');
        }

        // TODO:
        // identifier - идентификатор соц сети внутри умнико. Если мы хотим, чтобы и комменты и не комменты отображались как один канал, нам нужно сделать его идентификатором чата.
        // realId - идентификатор канала в умнико,
        // ID - идентификатор канала в соц сети
        // нам нужно понимать, в какой именно канал мы напишем

        return new Chat(
            externalId: $this->chatInstance() . ':' . $this->customerId(),
            messengerInstance: $this->messengerInstance(),
            contact: $this->customerId(),
            operator: $this->operatorId(),
            messengerId: $messengerId,
            extraData: [
                ExtraDataProps::LEAD_ID => data_get($this->data, 'leadId'),
                // we determine to which source we respond using this property
                // this will be reset on incoming/outgoing message. So we respond to the channel from which we caught last message.
                ExtraDataProps::CURRENT_REAL_ID => data_get($this->source, 'realId'),
                ExtraDataProps::SOURCES => json_encode($sources)
            ],
        );
    }

    public function customerId()
    {
        if (in_array($this->webhookRootObject(), ['message', 'lead'])) {
            return data_get($this->module->getApiCommands()->getLead($this->data['leadId']), 'customerId');
        }
        if ($this->webhookRootObject() === 'customer') {
            return data_get($this->data, 'customer.id');
        }
        throw new ErrorException(ErrorCode::DataMissing, "Can not retrieve customer instance from {$this->webhookRootObject()} event");
    }

    public function operatorId()
    {
        if (in_array($this->webhookRootObject(), ['message', 'lead'])) {
            return data_get($this->module->getApiCommands()->getLead($this->data['leadId']), 'userId');
        }
        if($this->webhookRootObject() === 'customer') {
            return data_get($this->module->getApiCommands()->getLead($this->data['customerId']), 'userId');
        }
        throw new ErrorException(ErrorCode::DataMissing, "Can not retrieve operator instance from {$this->webhookRootObject()} event");
    }

    public function chatInstance(): string
    {
        if (!in_array($this->webhookRootObject(), ['message', 'lead'])) {
            throw new ErrorException(ErrorCode::DataMissing, "Can not retrieve chat instance from {$this->webhookRootObject()} event");
        }
        $umnicoSocialIdentifier = data_get($this->source, 'identifier');
        return $umnicoSocialIdentifier;
    }

    #[ArrayShape([
        'id' => 'string',
        'realId' => 'int',
        'name' => 'string',
        'type' => 'string',
        'saId' => 'int',
        'sender' => 'string',
        'token' => 'string',
        'identifier' => 'string',
    ])]
    protected function setSource()
    {
        $this->source = match ($this->webhookRootObject()) {
            'message' => data_get(
                $this->data,
                'message.source',
            ),
            'lead' => $this->module->getApiCommands()->getSource(
                data_get($this->data, 'leadId'),
                data_get($this->data, 'lead.socialAccount.id')
            ),
            default => [],
        };
    }

    public function timestamp(): Carbon
    {
        return Carbon::now();
    }

    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset): mixed
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset): void
    {
        // TODO: Implement offsetUnset() method.
    }

    private function webhookRootObject(): string
    {
        return explode('.', data_get($this->data, 'type', 'error'))[0];
    }

    private function webhookEvent(): string
    {
        return explode('.', data_get($this->data, 'type', 'error'))[1];
    }
}
