<?php

namespace BmPlatform\Umnico\Utils;

class ExtraDataProps
{
    const LEAD_ID = 'leadId';
    const CURRENT_REAL_ID = 'currentRealId';
    const SOURCES = 'sources';
    const ADMIN_OPERATOR_ID = 'adminId';
    const TOKEN = 'token';
    const CONTACT_PROFILES = 'profiles';
    const ACCESS_TO_SOURCES = 'accessToSources';
    const EXTERNAL_METADATA = 'metadataFromExternalSystem';
}
