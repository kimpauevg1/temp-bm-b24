<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use Illuminate\Support\Facades\Config;

final class Utils
{
    public static function parseExternalId(string $externalId)
    {
        return explode(':', $externalId, 2);
    }

    public static function matchServer($apiUrl): ?string
    {
        foreach (Config::get('umnico.servers') as $id => $server) {
            if (preg_match("#^{$server['matcher']}$#", $apiUrl)) {
                return $id;
            }
        }

        return null;
    }
}