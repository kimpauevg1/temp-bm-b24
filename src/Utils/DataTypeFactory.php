<?php

namespace BmPlatform\Umnico\Utils;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\ApiCommands;

class DataTypeFactory
{
    public static function getContact(ApiCommands $api, string|array $externalContact, $messengerId = null): ?Contact
    {
        if (!$externalContact) {
            return null;
        }
        $contact = $externalContact;
        if (is_string($contact)) {
            $contact = $api->getContact($contact);
        }
        if (!$contact) {
            return null;
        }
        return new Contact(
            externalId: $externalContact,
            name: data_get($contact, 'name') ?? data_get($contact, 'login'),
            phone: data_get($contact, 'phone'),
            email: data_get($contact, 'email'),
            avatarUrl: data_get($contact, 'avatar'),
            extraFields: ['address' => data_get($contact, 'address'), 'login' => data_get($contact, 'login')],
            messengerId: $messengerId,
            extraData: Helpers::withoutEmptyStrings([ExtraDataProps::CONTACT_PROFILES => data_get($contact, 'profiles')])
        );
    }

    public static function getOperator(ApiCommands $api, string|array $externalOperator): ?Operator
    {
        if (!$externalOperator) {
            return null;
        }
        $operator = $externalOperator;
        if (is_string($operator)) {
            $operator = $api->getOperator($operator);
        }
        if (!$operator) {
            return null;
        }
        return new Operator(
            externalId: data_get($operator, 'id'),
            name: data_get($operator, 'name') ?? data_get($operator, 'login'),
            email: data_get($operator, 'login'),
            extraData: Helpers::withoutEmptyStrings([
                ExtraDataProps::ACCESS_TO_SOURCES => data_get($operator, 'sources'),
                // if the operator was imported from external system, we're gonna have info about the system
                ExtraDataProps::EXTERNAL_METADATA => data_get($operator, 'metadata'),
            ]),
        );
    }
}
