<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTags;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTickets;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\Commands\UpdatesContactData;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Umnico\Commands\MessageCommands;
use BmPlatform\Umnico\Commands\OperatorCommands;
use BmPlatform\Umnico\Commands\TagCommands;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\UmnicoStatusType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class Commands implements CommandsInterface, SendsSystemMessages, UpdatesContactData, TransfersChatToOperator,
                          SupportsChatTags, SupportsChatTickets
{
    use MessageCommands, OperatorCommands, TagCommands;

    public function __construct(public readonly AppHandler $module)
    {
    }

    public function updateContactData(UpdateContactDataRequest $request): void
    {
        $payload = [];

        $payload['name'] = $request->name;
        $payload['phone'] = $request->phone;
        $payload['email'] = $request->email;
        $payload = array_filter($payload);

        if (!$payload) {
            return;
        }

        $this->module->getApiClient()->put('customers/:customerId', [
            'params' => ['customerId' => $request->contact->getExternalId()],
            'json' => $payload,
        ]);
    }

    public function closeChatTicket(Chat $chat): void
    {
        $currentLead = $this->getCurrentLead($chat);
        $currentLeadStatus = $this->getCurrentLeadStatus($currentLead);

        if (!UmnicoStatusType::isActive(data_get($currentLeadStatus, 'type'))) {
            throw new ErrorException(ErrorCode::ChatTicketAlreadyClosed, 'Chat ticket is already closed.');
        }

        // Assign to admin if no operator provided
        if (!data_get($currentLead, 'userId')) {
            $currentLead['userId'] = data_get($this->module->user->getExtraData(), ExtraDataProps::ADMIN_OPERATOR_ID);
        }

        $completedStatus = $this->getCompletedStatus();

        $this->module->getApiClient()->put('leads/:id', [
            'params' => ['id' => data_get($currentLead, 'id')],
            'json' => ['statusId' => data_get($completedStatus, 'id'), 'userId' => $currentLead['userId']],
        ]);

        Event::dispatch(new ChatTicketClosed($this->module->user, $chat->getExternalId(), timestamp: Carbon::now()));
    }

    private function getCurrentLead(Chat $chat): ?array
    {
        $leadId = data_get($chat->getExtraData(), ExtraDataProps::LEAD_ID);
        if (!$leadId) {
            throw new ErrorException(ErrorCode::DataMissing, 'Can not get current lead\'s id');
        }
        return $this->module->getApiCommands()->getLead($leadId);
    }

    private function getCurrentLeadStatus(array $currentLead): ?array
    {
        $statusId = data_get($currentLead, 'statusId');
        if (!$statusId) {
            throw new ErrorException(ErrorCode::DataMissing, 'Can not get current lead\'s status id');
        }
        return $this->module->getApiCommands()->getStatus($statusId);
    }

    private function getCompletedStatus(): array
    {
        $completedStatus = $this->module->getApiCommands()->getStatusByType(UmnicoStatusType::completed);
        if (!$completedStatus) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Could not get completed status');
        }
        return $completedStatus;
    }
}
