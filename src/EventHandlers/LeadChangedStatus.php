<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\UmnicoStatusType;

class LeadChangedStatus
{
    public function __invoke(DataWrap $data)
    {
        if(!$this->isLeadActive($data)) {
            return new ChatTicketClosed($data->module->user, $data->chat()->externalId, (string)$data['leadId'], $data->timestamp());
        }
    }

    protected function isLeadActive(DataWrap $data): bool
    {
        $statusId = data_get($data, 'lead.statusId');
        if (!$statusId) {
            return false;
        }
        $status = $data->module->getApiCommands()->getStatus($statusId);
        return UmnicoStatusType::isActive(data_get($status, 'type', ''));
    }
}
