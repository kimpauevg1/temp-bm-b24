<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Umnico\Utils\DataWrap;

class MessageOutgoing extends MessageIncoming
{
    public function __invoke(DataWrap $data)
    {
        if (data_get($data, 'message.customId') === config('umnico_params.custom_id')) {
            return;
        }
        return new OutboxSent(
            moduleInstance: $data->module->user,
            chat: $data->chat(),
            message: $this->messageData($data),
            operator: $data->operatorId(),
            timestamp: $data->timestamp(),
        );
    }
}
