<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\Events\ContactDataUpdated;
use BmPlatform\Umnico\Utils\DataTypeFactory;
use BmPlatform\Umnico\Utils\DataWrap;

class CustomerChanged
{
    public function __invoke(DataWrap $data)
    {
        $contact = DataTypeFactory::getContact($data->module->getApiCommands(), $data->customerId());
        if (!$contact) {
            return null;
        }
        return new ContactDataUpdated(
            moduleInstance: $data->module->user,
            contact: $contact,
            timestamp: $data->timestamp()
        );
    }
}
