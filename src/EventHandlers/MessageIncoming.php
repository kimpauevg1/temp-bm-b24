<?php

namespace BmPlatform\Umnico\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Support\Helpers;
use BmPlatform\Umnico\Utils\DataTypeFactory;
use BmPlatform\Umnico\Utils\DataWrap;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\UmnicoMediaFileType;

class MessageIncoming
{
    public function __invoke(DataWrap $data)
    {
        $contact = DataTypeFactory::getContact(
            $data->module->getApiCommands(),
            $data->customerId()
        ) ?? $data->customerId();

        return new InboxReceived(
            moduleInstance: $data->module->user,
            chat: $data->chat(),
            participant: $contact,
            message: $this->messageData($data),
            externalItem: $this->socialPost($data),
            flags: $this->flags($data) ?: null,
            timestamp: $data->timestamp(),
        );
    }

    protected function socialPost(DataWrap $data): ?ExternalPlatformItem
    {
        if ($preview = data_get($data, 'message.preview', false)) {
            return new ExternalPlatformItem(
                externalId: data_get($data, 'message.source.type') . ':' . data_get($data, 'message.source.id'),
                url: data_get($preview, 'url'),
                extraData: Helpers::withoutEmptyStrings(['photo' => data_get($preview, 'photo')])
            );
        }

        return null;
    }

    protected function flags($data)
    {
        return data_get($data, 'message.preview', false) ? InboxFlags::EXTERNAL_POST_COMMENT : 0;
    }

    protected function messageData(DataWrap $data): MessageData
    {
        $extraData = [
            'chatExternalId' => $data->chatInstance(),
        ];

        return new MessageData(
            externalId: data_get($data, 'leadId') . ':' . data_get($data, 'message.messageId'),
            text: data_get($data, 'message.message.text'),
            attachments: $this->attachments($data),
            extraData: $extraData,
        );
    }

    protected function attachments(DataWrap $data): ?array
    {
        return collect($data['message.attachments'] ?? null)
            ->filter(function ($i) {
                return (!is_null(UmnicoMediaFileType::convertToInternal($i['type'])) && data_get($i, 'url', false));
            })
            ->map(fn($i) => new MediaFile(
                type: UmnicoMediaFileType::convertToInternal($i['type']),
                url: $i['url'],
            ))->all() ?: null;
    }
}
