<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Umnico\Utils\ExtraDataProps;
use BmPlatform\Umnico\Utils\Utils;
use Carbon\CarbonTimeZone;
use Illuminate\Support\Arr;

class Integrator
{
    public function __construct(public readonly array $data)
    {

    }

    public function __invoke(): AppIntegrationData
    {
        $apiClient = $this->createApiClient();
        $account = $apiClient->get('account/me');
        $managers = $apiClient->get('managers');

        $owner = Arr::collapse(Arr::where($managers, function ($value, $key) {
            return (data_get($value, 'role') == 'owner');
        }));

        return new AppIntegrationData(
            type: 'umnico',
            externalId: $this->serverId().':' . data_get($account, 'account.id'),
            name: data_get($owner, 'name', ''),
            locale: 'ru',
            timeZone: CarbonTimeZone::createFromHourOffset(3), //hardcoded moscow timezone, don't have such info from API
            email: data_get($owner, 'login', ''),
            extraData: [
                ExtraDataProps::TOKEN => data_get($this->data, 'token'),
                ExtraDataProps::ADMIN_OPERATOR_ID => data_get($owner, 'id'),
            ],
        );
    }

    protected function createApiClient(): ApiClient
    {
        return new ApiClient($this->data['api_domain'], $this->data['token']);
    }

    protected function serverId()
    {
        if ($server = Utils::matchServer($this->data['api_domain'])) return $server;

        throw new ErrorException(ErrorCode::IntegrationNotPossible, 'Api server is not registered');
    }
}
