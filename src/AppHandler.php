<?php

namespace BmPlatform\Umnico;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\Features\HasOperatorGroups;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\Features\HasTags;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\Utils\DataTypeFactory;
use BmPlatform\Umnico\Utils\Utils;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\App;
use RuntimeException;

class AppHandler implements AppHandlerInterface, HasOperators,
                            HasTags//, HasOperatorGroups TODO: I suppose Umnico doesn't support operator groups, I need to make sure.
{
    public function __construct(
        public readonly AppInstance $user,
        protected Repository $config,
        private ?ApiClient $apiClient = null,
        private ?Commands $commands = null,
        private ?ApiCommands $apiCommands = null,
    ) {
        //
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        // TODO: messenger instances don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect($this->getApiClient()->get('integrations'))->map(
                fn(array $remoteMessengerInstance) => new MessengerInstance(
                    externalType: $remoteMessengerInstance['type'],
                    externalId: $remoteMessengerInstance['id'],
                    name: $remoteMessengerInstance['login'],
                    messengerId: $remoteMessengerInstance['externalId'],
                    internalType: MessengerType::tryFrom(
                        $this->config->get(
                            'umnico.messenger_types.' . $remoteMessengerInstance['type'] . '.internal_type'
                        )
                    ),
                )
            )->all(), null
        );
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        // TODO: operators don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect(
                $this->getApiCommands()->getOperators()
            )->map(
                fn(array $remoteOperator) => DataTypeFactory::getOperator($this->getApiCommands(), $remoteOperator)
            )->all(), null
        );
    }

    public function getCommands(): \BmPlatform\Abstraction\Interfaces\Commands\Commands
    {
        if (isset($this->commands)) {
            return $this->commands;
        }

        return $this->commands = new Commands($this);
    }

    public function getApiClient(): ApiClient
    {
        if (isset($this->apiClient)) {
            return $this->apiClient;
        }

        [$server,] = Utils::parseExternalId($this->user->getExternalId());

        if (!$domain = $this->config->get("umnico.servers.{$server}.api_domain")) {
            throw new RuntimeException("Umnico server [$server] is not configured.");
        }

        return $this->apiClient = new ApiClient($domain, $this->user->getExtraData()['token'], [
            'app_instance' => $this->user,
        ]);
    }

    public function getTags(mixed $cursor = null): IterableData
    {
        // TODO: tags don't seem to have pagination in Umnico; make sure it's true
        return new IterableData(
            collect($this->getApiClient()->get('tags'))->map(
                fn(array $remoteTagInstance) => new Tag(
                    externalId: $remoteTagInstance['id'],
                    label: $remoteTagInstance['tag']
                )
            )->all(), null
        );
    }

    public function getApiCommands(): ApiCommands
    {
        if (isset($this->apiCommands)) {
            return $this->apiCommands;
        }

        return $this->apiCommands = new ApiCommands($this->getApiClient());
    }

    public function allowedTransports(): array
    {
        return array_keys($this->config->get('umnico.messenger_types'));
    }

    public function activate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))();
    }

    public function deactivate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))(enabled: false);
    }

    public function schema(): array
    {
        return $this->config->get('umnico');
    }
}
