<?php

use Illuminate\Support\Facades\Route;
use BmPlatform\Umnico\Http\IntegrationController;
use BmPlatform\Umnico\Http\CallbackController;

Route::namespace('BmPlatform\Bitrix24\Http')->name('bitrix24::')->prefix('integrations/bitrix24')->group(function () {
    Route::post('webhook', CallbackController::class)->name('callback');
    Route::get('/authenticate', [IntegrationController::class, 'authenticate'])->name('integration.authenticate');
    Route::get('/oauth', [IntegrationController::class, 'redirectToOauthLink'])->name('integration.oauth');
    Route::get('/', [IntegrationController::class, 'index'])->name('integration');
});

