<?php

namespace BmPlatform\Umnico\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\EventHandler;
use BmPlatform\Umnico\Utils\DataWrap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CallbackController
{
    public function __invoke(Request $request, Hub $hub, $callbackId)
    {
        Log::info('WH', $request->toArray());
        $app = $hub->findAppByCallbackId('umnico', $callbackId);
        (new EventHandler)(new DataWrap($app->getHandler(), $request->toArray()));
    }
}
