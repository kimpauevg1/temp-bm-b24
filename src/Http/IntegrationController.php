<?php

namespace BmPlatform\Umnico\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Umnico\Integrator;
use BmPlatform\Umnico\SetWebhook;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class IntegrationController
{
    public function __invoke(Request $request, Hub $hub, $apiToken = null)
    {
        if (!$apiToken) {
            return view('umnico::integrate');
        }
        // Make an app data that contains all info to create user on the other side
        $makeAppData = new Integrator(
            ['api_domain' => config('umnico.servers.ru.api_domain')] + ['token' => $apiToken]
        );

        // Make or update app instance from app data
        $instance = $hub->integrate($makeAppData());


//        // Set the webhook
        (new SetWebhook($instance->getHandler(), $hub))();

//        // Redirect user to the web interface
        return new RedirectResponse($hub->webUrl($hub->getAuthToken($instance)));
    }

    public function index(): View
    {
        return view('bitrix24::integrate');
    }

    public function redirectToOauthLink(Request $request): RedirectResponse
    {
        $params = [
            'client_id' => $request->input('client_id'),
            'state' => json_encode([
                'client_id' => $request->input('client_id'),
                'client_secret' => $request->input('client_secret'),
            ]),
        ];

        $url = $request->input('portal_url') . '/oauth/authorize/?' . http_build_query($params);

        return redirect($url);
    }

    public function authenticate(Request $request): JsonResponse
    {
        Log::info(json_encode($request->all()));

        return response()->json($request->all());
    }
}
