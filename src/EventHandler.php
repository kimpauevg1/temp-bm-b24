<?php

namespace BmPlatform\Umnico;

use BmPlatform\Umnico\Utils\DataWrap;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

class EventHandler
{
    public function __construct()
    {
        //
    }

    public function __invoke(DataWrap $data)
    {
        $class = $this->getClass(data_get($data, 'type', ''));

        if (!class_exists($class)) {
            return null;
        }

        $result = (new $class)($data);

        if (!$result instanceof \Traversable) {
            $result = [ $result ];
        }

        foreach ($result as $event) {
            if($event) {
                Event::dispatch($event);
            }
        }
    }

    private function getClass($webhookType)
    {
        return 'BmPlatform\Umnico\EventHandlers\\' . Str::studly(Str::replace('.', '_', $webhookType));
    }
}
